# ProxyTools
A simple library to configure system proxy settings.

## Usages
~~~ts
import { getProxyManager, ProxyStack } from "@takanashi-hosh1no/proxytools"

let pm = getProxyManager()
let proxy = new ProxyStack("socks.proxy.com", 10808, "http.proxy.com", 8080)
// or
// let proxy = new ProxyStack("socks.proxy.com", 10808, "", -1)
// let proxy = new ProxyStack("", -1, "http.proxy.com", 8080)

// apply proxy settings to system
pm.setProxy(proxy)

// clear proxy settings
pm.clearProxy()

~~~

## Credits
[Qv2ray](https://github.com/Qv2ray/Qv2ray/blob/2b9a5ecc239587517b34af123e4d04df8a7badf9/src/components/proxy/QvProxyConfigurator.cpp) 
for modify proxy setting on GNOME or KDE  
[v2rayA](https://github.com/v2rayA/v2rayA/blob/8ce1895b1fe8a8f559651f23a1a69c2bf47190f3/service/core/iptables/systemProxy_macos.go) 
for modify proxy setting on MacOS