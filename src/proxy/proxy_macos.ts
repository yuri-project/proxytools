import { execSync } from "child_process"
import ProxyManager = require("../ProxyManager");
import ProxyStack = require("../ProxyStack")

export class MacOSProxyManager extends ProxyManager {
    setProxy(proxy: ProxyStack) {
        let services = this.getNetworkService()
        services.forEach(service => {
            execSync(`/usr/sbin/networksetup -setautoproxystate ${service} off`)
            if (proxy.hasProxyHttp()) {
                execSync(`/usr/sbin/networksetup -setwebproxystate ${service} on`)
                execSync(`/usr/sbin/networksetup -setsecurewebproxystate ${service} on`)
                execSync(`/usr/sbin/networksetup -setwebproxy ${service} ${proxy.http_address} ${proxy.http_port}`)
                execSync(`/usr/sbin/networksetup -setsecurewebproxy ${service} ${proxy.http_address} ${proxy.http_port}`)
            }
            if (proxy.hasProxySocks()) {
                execSync(`/usr/sbin/networksetup -setsocksfirewallproxystate ${service} on`)
                execSync(`/usr/sbin/networksetup -setsocksfirewallproxystate ${service} ${proxy.socks_address} ${proxy.socks_port}`)
            }
        });
    }

    clearProxy() {
        let services = this.getNetworkService()
        services.forEach(service => {
            execSync(`/usr/sbin/networksetup -setautoproxystate ${service} off`)
            execSync(`/usr/sbin/networksetup -setwebproxystate ${service} off`)
            execSync(`/usr/sbin/networksetup -setsecurewebproxystate ${service} off`)
            execSync(`/usr/sbin/networksetup -setsocksfirewallproxystate ${service} off`)
        });
    }

    getNetworkService(): Array<string> {
        let lines = execSync("/usr/sbin/networksetup -listallnetworkservices").toString().split("\n")
        let services = []
        for (let i = 0; i < lines.length; i++) {
            const element = lines[i].trim();
            if (i == 0 || element.includes("*")) {
                continue
            }
            services.push(element)
        }
        return services
    }
}