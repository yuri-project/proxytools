import { determinePlatform, Platform } from "../platform"
import { MacOSProxyManager } from "./proxy_macos"
import { WindowsProxyManager } from "./proxy_windows"
import { LinuxProxyManager, GNOMEProxyManager, KDEProxyManager } from "./proxy_linux" 
import ProxyManager = require("../ProxyManager")

export function getProxyManagerByPlatform(platform: Platform): ProxyManager {
    if (platform == Platform.WINDOWS) {
        return new WindowsProxyManager()
    } else if (platform == Platform.MAC_OS) {
        return new MacOSProxyManager()
    } else if (platform == Platform.LINUX_GNOME) {
        return new GNOMEProxyManager()
    } else if (platform == Platform.LINUX_KDE) {
        return new KDEProxyManager()
    } else if (platform == Platform.LINUX_XFCE || platform == Platform.LINUX_OTHER) {
        return new LinuxProxyManager()
    }

    return new ProxyManager()
}

export function getProxyManager(): ProxyManager {
    return getProxyManagerByPlatform(determinePlatform())
}