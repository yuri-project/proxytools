import { execSync } from "child_process"
import * as setenv from "setenv"
import ProxyManager = require("../ProxyManager")
import ProxyStack = require("../ProxyStack")

export class GNOMEProxyManager extends ProxyManager {
    setProxy(proxy: ProxyStack) {
        execSync('gsettings set org.gnome.system.proxy mode "manual"')
        if (proxy.hasProxyHttp()) {
            execSync(`gsettings set org.gnome.system.proxy.http host "${proxy.http_address}"`)
            execSync(`gsettings set org.gnome.system.proxy.https host "${proxy.http_address}"`)
            execSync(`gsettings set org.gnome.system.proxy.http port ${proxy.http_port}`)
            execSync(`gsettings set org.gnome.system.proxy.https port ${proxy.http_port}`)
        }
        if (proxy.hasProxySocks()) {
            execSync(`gsettings set org.gnome.system.proxy.socks host "${proxy.socks_address}"`)
            execSync(`gsettings set org.gnome.system.proxy.socks port ${proxy.socks_port}`)
        }
    }

    clearProxy() {
        execSync('gsettings set org.gnome.system.proxy mode "none"')
    }
}

export class KDEProxyManager extends ProxyManager {
    setProxy(proxy: ProxyStack) {
        execSync('kwriteconfig5 --file ~/.config/kioslaverc --group "Proxy Settings" --key "ProxyType" 1')
        if (proxy.hasProxyHttp()) {
            let addr = proxy.proxyHttp()
            execSync(`kwriteconfig5 --file ~/.config/kioslaverc --group "Proxy Settings" --key "httpProxy" "${addr}"`)
            execSync(`kwriteconfig5 --file ~/.config/kioslaverc --group "Proxy Settings" --key "httpsProxy" "${addr}"`)
        }
        if (proxy.hasProxySocks()) {
            let addr = proxy.proxySocks()
            execSync(`kwriteconfig5 --file ~/.config/kioslaverc --group "Proxy Settings" --key "socksProxy" "${addr}"`)
        }
        // Notify kioslaves to reload system proxy configuration.
        execSync("dbus-send --type=signal /KIO/Scheduler org.kde.KIO.Scheduler.reparseSlaveConfiguration string:''")
    }

    clearProxy() {
        execSync('kwriteconfig5 --file ~/.config/kioslaverc --group "Proxy Settings" --key "ProxyType" 0')
    }
}

export class LinuxProxyManager extends ProxyManager {
    setProxy(proxy: ProxyStack) {
        this.clearProxy()
        if (proxy.hasProxyHttp()) {
            let addr = proxy.proxyHttp()
            setenv.set("HTTP_PROXY", addr)
            setenv.set("HTTPS_PROXY", addr)
        }
        if (proxy.hasProxySocks()) {
            let addr = proxy.proxySocks()
            setenv.set("SOCKS_PROXY", addr)
        }
    }

    clearProxy() {
        setenv.set("HTTP_PROXY", "")
        setenv.set("HTTPS_PROXY", "")
        setenv.set("SOCKS_PROXY", "")
    }
}