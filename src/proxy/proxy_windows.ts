import { execSync } from "child_process"
import winconfigure = require("./winconfigure")
import path = require("path")
import ProxyManager = require("../ProxyManager")
import ProxyStack = require("../ProxyStack")

export class WindowsProxyManager extends ProxyManager {
    setProxy(proxy: ProxyStack) {
        if (proxy.hasProxyHttp()) {
            let addr = ""
            if (proxy.http_address.includes(":")) { // ipv6
                addr = `[${proxy.http_address}]:${proxy.http_port}`
            } else {
                addr = `${proxy.http_address}:${proxy.http_port}`
            }
            this.runConfigurator("set " + addr)
        } else {
            throw new Error("Windows only supports HTTP proxies");
        }
    }

    clearProxy() {
        this.runConfigurator("clear")
    }

    runConfigurator(command: string) {
        let exePath = path.join(process.env.APPDATA, "proxytools_configurator.exe")
        // write the native configurator exe to appdata
        winconfigure(exePath)
        // run the configurator
        execSync(`"${exePath}" ${command}`)
    }
}