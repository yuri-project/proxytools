class ProxyStack {
    socks_address: string = "";
    socks_port: number = -1;
    http_address: string = "";
    http_port: number = -1;

    // constructor(type: string, address: string, port: number) {
    //     if (type == "http") {
    //         this.http_address = address
    //         this.http_port = port
    //     } else if (type == "socks") {
    //         this.socks_address = address
    //         this.socks_port = port
    //     } else {
    //         throw new Error(`Invalid proxy type: ${type}`);
    //     }
    // }

    constructor(socks_address: string, socks_port: number, http_address: string, http_port: number) {
        this.socks_address = socks_address
        this.socks_port = socks_port
        this.http_address = http_address
        this.http_port = http_port
    }

    hasProxyHttp(): boolean {
        return this.http_address != "" && this.http_port >= 0 && this.http_port <= 65535
    }

    hasProxySocks(): boolean {
        return this.socks_address != "" && this.socks_port >= 0 && this.socks_port <= 65535
    }

    proxyHttp(): string {
        if (this.http_address.includes(":")) { // ipv6
            return `http://[${this.http_address}]:${this.http_port}`
        } else {
            return `http://${this.http_address}:${this.http_port}`
        }
    }

    proxySocks(): string {
        if (this.socks_address.includes(":")) { // ipv6
            return `socks://[${this.socks_address}]:${this.socks_port}`
        } else {
            return `socks://${this.socks_address}:${this.socks_port}`
        }
    }
}

export = ProxyStack;