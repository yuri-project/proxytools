import { platform } from "os"
import { env } from "process"

export enum Platform {
    WINDOWS = "Windows",
    MAC_OS = "MacOS",
    LINUX_GNOME = "GNOME",
    LINUX_KDE = "KDE",
    LINUX_XFCE = "XFCE",
    LINUX_OTHER = "Linux", // or unix
}

export function determinePlatform(): Platform {
    let pf: string = platform()

    // https://nodejs.org/api/os.html#osplatform
    // Returns a string identifying the operating system platform for which the Node.js binary was compiled.
    // The value is set at compile time.
    // Possible values are 'aix', 'darwin', 'freebsd', 'linux', 'openbsd', 'sunos', and 'win32'.

    if (pf.includes("win")) {
        return Platform.WINDOWS
    } else if (pf == "darwin") {
        return Platform.MAC_OS
    } else { // linux
        let session = env["XDG_SESSION_DESKTOP"] // desktop env
        if (typeof session == "string") {
            session = session.toLowerCase()
            if (session == "kde" || session == "plasma") {
                return Platform.LINUX_KDE
            } else if (session == "gnome") {
                return Platform.LINUX_GNOME
            } else if (session == "xfce") {
                return Platform.LINUX_XFCE
            }
        }
        return Platform.LINUX_OTHER
    }
}