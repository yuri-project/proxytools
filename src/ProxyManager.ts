import ProxyStack = require("./ProxyStack");

class ProxyManager {
    setProxy(proxy: ProxyStack) {
        throw new Error("Not implemented");
    }

    clearProxy() {
        throw new Error("Not implemented");
    }
}

export = ProxyManager;