const fs = require('fs');
const zlib = require('zlib');

const path = 'proxytools_winconfigure.exe';
fs.writeFileSync("winconfigure.js", `
const fs = require('fs');
const zlib = require('zlib');
module.exports = function(path) {
    fs.writeFileSync(path, zlib.unzipSync(Buffer.from('${zlib.deflateSync(fs.readFileSync(path)).toString('base64')}', 'base64')));
}
`)


