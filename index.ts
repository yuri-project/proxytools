import ProxyManager = require("./src/ProxyManager");
import ProxyStack = require("./src/ProxyStack");
import { determinePlatform, Platform } from "./src/platform"
import { getProxyManagerByPlatform, getProxyManager } from "./src/proxy/proxy";

export { ProxyManager, ProxyStack, Platform, determinePlatform, getProxyManagerByPlatform, getProxyManager };